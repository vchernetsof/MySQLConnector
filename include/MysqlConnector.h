#ifndef MYSQL_MYSQLCONNECTOR_H
#define MYSQL_MYSQLCONNECTOR_H

#include <mysql_connection.h>
#include <mysql_driver.h>
#include <cppconn/statement.h>
#include <cppconn/resultset.h>
#include <string>
#include <iostream>
#include <vector>
#include <map>

#include "QueryBuilder.h"
#include "../include/helpers/ResultSelect.h"


/**
 * Интерфейс соединения с СУБД MySQL
 */
class MysqlConnector
{
private:
    sql::Driver *driver;
    sql::Connection *connection;
    sql::Statement *statement;
public:
    MysqlConnector(const std::string &host, const std::string &login, const std::string &pswd, const std::string &schema);

private:
    /**
     * Выполнение запроса
     * {@see https://dev.mysql.com/doc/connector-cpp/en/connector-cpp-examples-results.html}
     * @param command
     * @return std::shared_ptr<sql::ResultSet>
     */
    sql::ResultSet* execute(const std::string &command);
public:
    ResultSelect* select(QueryBuilder *queryBuilder);
};

#endif //MYSQL_MYSQLCONNECTOR_H
