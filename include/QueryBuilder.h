#ifndef MYSQL_QUERYBUILDER_H
#define MYSQL_QUERYBUILDER_H

#include <vector>
#include <iterator>
#include <sstream>
#include <string>
#include <map>
#include "components/Operator.h"
#include "components/SelectOperation.h"
#include "components/FromOperator.h"
#include "components/WhereOperator.h"
#include "components/AndOperator.h"
#include "components/OrOperator.h"

/**
 * Билдер запросов
 */
class QueryBuilder
{
private:
    /**
     * Вектор операторов
     */
    std::vector<Operator*> _query;

    /**
     * Вектор полей, учавствующих в выборке
     */
    std::vector<std::string> _fields;
public:
    /**
     * Оператор 'SELECT'
     * @param fields
     * @return QueryBuilder
     */
    QueryBuilder* select(const std::vector<std::string> &fields);

    /**
     * Оператор 'FROM'
     * @param tableName
     * @return QueryBuilder
     */
    QueryBuilder* from(const std::string &tableName);

    /**
     * Оператор 'WHERE'
     * @param expression
     * @return QueryBuilder
     */
    QueryBuilder* where(const std::string &expression);

    /**
     * Оператор 'AND'
     * @param expression
     * @return QueryBuilder
     */
    QueryBuilder* _and(const std::string &expression);

    /**
     * Оператор 'OR'
     * @param expression
     * @return QueryBuilder
     */
    QueryBuilder* _or(const std::string &expression);

    /**
     * Временный метод, для проверки корректности сборки запроса
     * @return std::string
     */
    std::string toString();

    /**
     * Поучение полей, участвующих в выборке
     * @return
     */
    std::vector<std::string> getFields();
};

#endif //MYSQL_QUERYBUILDER_H
