#ifndef MYSQL_ANDOPERATOR_H
#define MYSQL_ANDOPERATOR_H

#include "Operator.h"
#include "Expression.h"

/**
 * Оператор 'AND'
 */
class AndOperator: public Operator
{
private:
    Expression *_expression;
public:
    explicit AndOperator(const std::string &expression);

    std::string get() override ;
};

#endif //MYSQL_ANDOPERATOR_H
