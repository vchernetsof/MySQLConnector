#ifndef MYSQL_EXPRESSION_H
#define MYSQL_EXPRESSION_H

#include <string>

/**
 * Выражение для {@see WhereOperator}
 */
class Expression
{
private:
    std::string _operation;
public:
    explicit Expression(const std::string &operation);

    std::string get();
};

#endif //MYSQL_EXPRESSION_H
