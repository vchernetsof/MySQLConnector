#ifndef MYSQL_FROMOPERATOR_H
#define MYSQL_FROMOPERATOR_H

#include "Operator.h"
#include <string>

class FromOperator: public Operator
{
private:
    std::string _tableName;

public:
    explicit FromOperator(const std::string &tableName);
    std::string get() override;
};

#endif //MYSQL_FROMOPERATOR_H
