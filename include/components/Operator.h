#ifndef MYSQL_OPERATOR_H
#define MYSQL_OPERATOR_H

#include <string>
#include <vector>
#include <sstream>

/**
 * Оператор
 */
class Operator
{
protected:
    /**
     * Операция
     */
    std::string _operation;
public:
    /**
     * @param operation
     * @param values
     */
    Operator() = default;;

    /**
     * Получение операции
     * @return std::string
     */
    virtual std::string get() = 0;

    std::string getOperation();
};

#endif //MYSQL_OPERATOR_H
