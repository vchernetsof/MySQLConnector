#ifndef MYSQL_OROPERATOR_H
#define MYSQL_OROPERATOR_H

#include "Expression.h"
#include "Operator.h"
#include <string>

/**
 * Оператор 'OR'
 */
class OrOperator: public Operator
{
private:
    Expression *_expression;
public:
    explicit OrOperator(const std::string &expression);

    std::string get() override;
};

#endif //MYSQL_OROPERATOR_H
