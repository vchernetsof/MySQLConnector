#ifndef MYSQL_SELECTOPERATION_H
#define MYSQL_SELECTOPERATION_H

#include "Operator.h"
#include "../../include/helpers/Formatter.h"

#include <string>
#include <vector>
#include <numeric>
#include <iterator>

/**
 * Оператор 'SELECT'
 */
class SelectOperation: public Operator
{
private:

    /**
     * Поля, участвующие в выборке
     */
    std::vector<std::string> _fields;
public:
    explicit SelectOperation(const std::vector<std::string> &fields);

    /**
     * Получение оператора в {@see std::string}
     * @return std::string
     */
    std::string get() override;
};

#endif //MYSQL_SELECTOPERATION_H
