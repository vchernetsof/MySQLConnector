#ifndef MYSQL_WHEREOPERATOR_H
#define MYSQL_WHEREOPERATOR_H

#include "Operator.h"
#include "Expression.h"

/**
 * Оператор 'WHERE'
 */
class WhereOperator: public Operator
{
private:
    Expression *_expression;
public:
    explicit WhereOperator(const std::string &expression);

    /**
     * Получение вырадежения в {@see std::string}
     * @return
     */
    std::string get() override ;
};

#endif //MYSQL_WHEREOPERATOR_H
