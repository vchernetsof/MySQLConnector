#ifndef MYSQL_OPERATOREXCEPTION_H
#define MYSQL_OPERATOREXCEPTION_H

#include <exception>
#include <string>
#include <sstream>

/**
 * Обработка исключений при составлении запроса
 */
class OperatorException: public std::exception
{
    virtual const char* what();
private:
    std::string _operatorName;

public:
    explicit OperatorException(const std::string &operatorName);
};

#endif //MYSQL_OPERATOREXCEPTION_H
