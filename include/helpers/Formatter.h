#ifndef MYSQL_FORMATTER_H
#define MYSQL_FORMATTER_H

#include <string>
#include <numeric>
#include <vector>

class Formatter
{
public:
    static std::string implode(const std::vector<std::string> &fields);
};

#endif //MYSQL_FORMATTER_H
