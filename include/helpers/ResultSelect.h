#ifndef MYSQL_RESULTSELECT_H
#define MYSQL_RESULTSELECT_H

#include <map>
#include <vector>
#include <string>

typedef std::vector<std::map<std::string, std::string>> table;

class ResultSelect
{
private:
    table result;
public:
    ResultSelect() = default;
    void setRow(const std::map<std::string, std::string> &row);
    table get();
};

#endif //MYSQL_RESULTSELECT_H
