#include "../include/MysqlConnector.h"

MysqlConnector::MysqlConnector(const std::string &host, const std::string &login,
                               const std::string &pswd, const std::string &schema) {
    driver = get_driver_instance();

    connection = driver->connect(host, login, pswd);

    connection->setSchema(schema);

    statement = connection->createStatement();
}

sql::ResultSet* MysqlConnector::execute(const std::string &command) {
    try {
        return statement->executeQuery(command);
    } catch (sql::SQLException &exception) {
        std::cerr << "#ERROR: SQLException in " << __FILE__ << std::endl;
        std::cerr << "(" << __FUNCTION__ << ") on line " << __LINE__ << std::endl;
        std::cerr << "#ERROR: " << exception.what() << std::endl;
        std::cerr << "(MySQL error code: " << exception.getErrorCode() << std::endl;
        std::cerr << "SQLState: " << exception.getSQLState() << ")" << std::endl;
    }
}

ResultSelect* MysqlConnector::select(QueryBuilder *queryBuilder) {
    auto *resultSelect = new ResultSelect();

    auto fields = queryBuilder->getFields();
    auto query = queryBuilder->toString();

    auto resultQuery = execute(query);

    while (resultQuery->next()) {
        std::map<std::string, std::string> row;

        for (auto &field: fields) {
            row.insert(std::pair<std::string, std::string>(field, resultQuery->getString(field)));
        }

        resultSelect->setRow(row);
    }

    return resultSelect;
}