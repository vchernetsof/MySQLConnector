#include "../include/QueryBuilder.h"
#include "../include/exceptions/OperatorException.h"

QueryBuilder* QueryBuilder::select(const std::vector<std::string> &fields) {
    _fields = fields;
    _query.push_back(new SelectOperation(fields));

    return this;
}


std::string QueryBuilder::toString() {
    std::string stringQuery;

    for (auto &_operator: _query) {
        stringQuery += _operator->get();
    }

    return stringQuery;
}

QueryBuilder* QueryBuilder::from(const std::string &tableName) {
    _query.push_back(new FromOperator(tableName));

    return this;
}

QueryBuilder* QueryBuilder::where(const std::string &expression) {
    _query.push_back(new WhereOperator(expression));

    return this;
}

QueryBuilder* QueryBuilder::_and(const std::string &expression) {
    _query.push_back(new AndOperator(expression));

    return this;
}

QueryBuilder* QueryBuilder::_or(const std::string &expression) {
    _query.push_back(new OrOperator(expression));

    return this;
}

std::vector<std::string> QueryBuilder::getFields() {
    return _fields;
}