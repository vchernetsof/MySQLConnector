#include "../../include/components/AndOperator.h"

AndOperator::AndOperator(const std::string &expression) {
    _operation = " AND ";
    _expression = new Expression(expression);
}

std::string AndOperator::get() {
    std::stringstream ss;

    ss << this->getOperation() << _expression->get();

    return static_cast<std::string>(ss.str());
}