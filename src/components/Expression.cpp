#include "../../include/components/Expression.h"

Expression::Expression(const std::string &operation) {
    _operation = operation;
}

std::string Expression::get() {
    return _operation;
}