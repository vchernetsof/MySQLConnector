#include "../../include/components/FromOperator.h"

FromOperator::FromOperator(const std::string &tableName) {
    _tableName = tableName;
    _operation = "FROM";
}

std::string FromOperator::get() {
    std::stringstream ss;

    ss << " " << getOperation() << " " << "`" +  _tableName + "`";

    return static_cast<std::string>(ss.str());
}