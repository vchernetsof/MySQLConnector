#include "../../include/components/OrOperator.h"

OrOperator::OrOperator(const std::string &expression) {
    _operation = " OR ";
    _expression = new Expression(expression);
}

std::string OrOperator::get() {
    std::stringstream ss;

    ss << this->getOperation() << _expression->get();

    return static_cast<std::string>(ss.str());
}