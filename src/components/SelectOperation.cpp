#include "../../include/components/SelectOperation.h"

SelectOperation::SelectOperation(const std::vector<std::string> &fields) {
    _operation = "SELECT";
    _fields = fields;
}

std::string SelectOperation::get() {
    std::stringstream ss;

    ss << this->getOperation() << " " << Formatter::implode(_fields);

    return static_cast<std::string>(ss.str());
}