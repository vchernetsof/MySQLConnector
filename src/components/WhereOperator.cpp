#include "../../include/components/WhereOperator.h"

WhereOperator::WhereOperator(const std::string &expression) {
    _operation = " WHERE ";
    _expression = new Expression(expression);
}

std::string WhereOperator::get() {
    std::stringstream ss;

    ss << this->getOperation() << _expression->get();

    return static_cast<std::string>(ss.str());
}