#include "../../include/exceptions/OperatorException.h"

const char* OperatorException::what() {
    std::ostringstream stringStream;

    stringStream << "ERROR: Operator " << _operatorName << " is not exist" << std::endl;

    std::string resultString = stringStream.str();

    return resultString.c_str();
}

OperatorException::OperatorException(const std::string &operatorName) {
    _operatorName = operatorName;
}