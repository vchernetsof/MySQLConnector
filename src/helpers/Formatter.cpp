#include "../../include/helpers/Formatter.h"

std::string Formatter::implode(const std::vector<std::string> &fields) {
    return std::accumulate(fields.begin(), fields.end(), std::string(),
                           [](const std::string &begin, const std::string &end) -> std::string {
                               return begin + (begin.length() > 0 ?  ", `" : "`") + end + "`";
                           });
}